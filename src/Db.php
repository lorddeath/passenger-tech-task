<?php

namespace Aziraphale\PassengerTechTask;

use Aura\Sql\ExtendedPdo;

/**
 * Very simple class to make our database connection (ExtendedPdo instance)
 *  easily accessible everywhere without relying on global variables.
 * `ExtendedPdo` extends the native `PDO` class.
 *
 * @package Aziraphale\PassengerTechTask
 * @link https://github.com/auraphp/Aura.Sql
 */
class Db
{
    protected static ExtendedPdo $instance;
    
    /**
     * Initialises an ExtendedPdo object (from the Aura.Sql library) with the
     *  passed connection details. Note that ExtendedPdo DOES NOT connect
     *  immediately. The connection will only be established once a method is
     *  called that requires it, or the `connect()` method is called.
     *
     * @param string      $dsn
     * @param string|null $user
     * @param string|null $pass
     * @return ExtendedPdo
     */
    public static function init(string $dsn, string $user = null, string $pass = null): ExtendedPdo
    {
        static::$instance = new ExtendedPdo($dsn, $user, $pass);
        return static::$instance;
    }
    
    /**
     * @return ExtendedPdo
     */
    public static function get(): ExtendedPdo
    {
        if (!isset(static::$instance)) {
            throw new \RuntimeException(
                sprintf('%1$s::init() must be called before %1$s::get()', static::class)
            );
        }
        return static::$instance;
    }
}
