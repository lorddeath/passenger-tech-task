<?php

namespace Aziraphale\PassengerTechTask\Helper;

class ConsoleHelper
{
    /**
     * Sets up PHP so that shutdown functions (via
     *  `register_shutdown_function()`) and class destructors are run when a
     *  SIGINT/SIGTERM signal is received (on *nix) or when CTRL+C is pressed
     *  (on Windows).
     * (PHP's standard behaviour is to exit immediately upon receiving
     *  SIGINT/SIGTERM/CTRL+C, *NOT* calling shutdown functions or class
     *  destructors, perhaps surprisingly!)
     * Returns false if the required `pcntl` (*nix)/`sapi_windows` functions
     *  aren't available.
     *
     * @return bool Whether a signal/CTRL+C handler was set.
     * @noinspection PhpComposerExtensionStubsInspection
     */
    public static function setupCleanExitOnSigintAndSigterm(): bool
    {
        if (function_exists('pcntl_async_signals')) {
            // *nix/POSIX-type environment
            // We need `pcntl_async_signals(true)` for signal handlers to
            //  behave properly on *nix. Without this, program execution
            //  continues despite SIGINT/SIGTERM being received, behaving as
            //  if `pcntl_signal()` was configured to *ignore* signals...
            pcntl_async_signals(true);
            
            $signalHandler = function (int $signo, $siginfo) {
                // `exit()` is enough to trigger shutdown/destructor funcs.
                exit();
            };
            pcntl_signal(SIGINT, $signalHandler);
            pcntl_signal(SIGTERM, $signalHandler);
            return true;
        }
    
        if (function_exists('sapi_windows_set_ctrl_handler')) {
            // Windows
            sapi_windows_set_ctrl_handler(function (int $event) {
                // `exit()` is enough to trigger shutdown/destructor funcs.
                exit();
            });
            return true;
        }
    
        // Neither pcntl nor Windows SAPI functions available
        return false;
    }
}
