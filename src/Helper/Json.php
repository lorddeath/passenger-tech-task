<?php

namespace Aziraphale\PassengerTechTask\Helper;

class Json
{
    /**
     * A wrapper for json_encode() with some "nicer" default behaviours.
     *
     * Default flags are:
     *  JSON_THROW_ON_ERROR,
     *  JSON_BIGINT_AS_STRING,
     *  JSON_UNESCAPED_SLASHES,
     *  JSON_UNESCAPED_UNICODE,
     *  JSON_PRESERVE_ZERO_FRACTION,
     *  JSON_FORCE_OBJECT (if $forceObject is true),
     *  JSON_PRETTY_PRINT (if $prettyPrint is true).
     *
     * @param mixed $input
     * @param bool  $forceObject
     * @param bool  $prettyPrint
     * @return string
     */
    public static function encode(mixed $input, bool $forceObject = false, bool $prettyPrint = false): string
    {
        // Define our preferred json_encode() flags...
        $flags =
            // Outputs an object rather than an array when a non-associative array is used (if $forceObject is true).
            //  Especially useful when the recipient of the output is expecting an object and the array is empty.
            ($forceObject ? JSON_FORCE_OBJECT : 0) |
            
            // Outputs "pretty" formatted JSON
            ($prettyPrint ? JSON_PRETTY_PRINT : 0) |
            
            // Encodes large integers as their original string value
            JSON_BIGINT_AS_STRING |
            
            // Don't escape `/`
            JSON_UNESCAPED_SLASHES |
            
            // Encode multibyte Unicode characters literally; default is to escape as \uXXXX
            JSON_UNESCAPED_UNICODE |
    
            // "Ensures that float values are always encoded as a float value."
            // i.e. ensures that a PHP float with no value after the decimal point is encoded as a float, with a trailing `.0`, instead of being encoded as an int
            JSON_PRESERVE_ZERO_FRACTION |
            
            // Throw a JsonException on an error
            JSON_THROW_ON_ERROR
            ;
    
        return json_encode($input, $flags);
    }
}
