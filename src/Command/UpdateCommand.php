<?php

namespace Aziraphale\PassengerTechTask\Command;

use Aziraphale\PassengerTechTask\Data\Postcode;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class UpdateCommand extends Command
{
    protected static $defaultName = 'postcode:update';
    //protected static $defaultName = 'update';
    
    protected function configure()
    {
        $this->setDescription("Fetches updated postcodes and loads them into the database.");
    }
    
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        
        try {
            $io->title("Postcode Database Update");
            
            $postcode = new Postcode($input, $output, $io);
            $postcode->downloadAndImportData();
            
            $io->success("Import complete");
            
            return Command::SUCCESS;
        } catch (\Throwable $ex) {
            $io->error($ex->getMessage());
            return Command::FAILURE;
        }
    }
}
