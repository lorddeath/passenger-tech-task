<?php

namespace Aziraphale\PassengerTechTask\Command;

use Aziraphale\PassengerTechTask\Data\Postcode;
use Aziraphale\PassengerTechTask\Helper\Json;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class NearbyPostcodesCommand extends Command
{
    protected static $defaultName = 'postcode:nearby';
    //protected static $defaultName = 'nearby';
    
    protected function configure()
    {
        $this
            ->setDescription("Finds postcodes within a set distance of a lat+lon coordinate, returning them as JSON.")
            
            ->addArgument('lat', InputArgument::REQUIRED,
                          "Latitude coordinate component.")
            ->addArgument('lon', InputArgument::REQUIRED,
                          "Longitude coordinate component.")
            ->addArgument('distance', InputArgument::OPTIONAL,
                          "Radius (metres) in which to search.", 500)
            
            ->addOption('pretty', 'p', InputOption::VALUE_NONE,
                        "Pretty-print JSON.")
            ->addOption('limit', 'l', InputOption::VALUE_REQUIRED,
                        "Max number of results to return.", 20)
            ->addOption('offset', 'o', InputOption::VALUE_REQUIRED,
                        "Value for MySQL's `OFFSET`.", 0)
        ;
    }
    
    protected function interact(InputInterface $input, OutputInterface $output)
    {
        // TODO Maybe prompt user for arguments?
    }
    
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        
        try {
            // Intentionally not outputting a title so this command's JSON
            //  output can be piped to another command.
            // Writing the title to stderr is an option once I figure out how
            //  to do that - and ideally only if stderr is a tty...
            //$io->title("Nearby Search");
            
            $postcode = new Postcode($input, $output, $io);
            $matches = $postcode->findNearby(
                latitude: $input->getArgument('lat'),
                longitude: $input->getArgument('lon'),
                distance: $input->getArgument('distance'),
                limit: $input->getOption('limit'),
                offset: $input->getOption('offset'),
            );
            
            $jsonString = Json::encode(
                $matches,
                prettyPrint: $input->getOption('pretty')
            );
            $io->text($jsonString);
            
            return Command::SUCCESS;
        } catch (\Throwable $ex) {
            $io->error($ex->getMessage());
            return Command::FAILURE;
        }
    }
}
