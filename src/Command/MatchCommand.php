<?php

namespace Aziraphale\PassengerTechTask\Command;

use Aziraphale\PassengerTechTask\Data\Postcode;
use Aziraphale\PassengerTechTask\Helper\Json;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class MatchCommand extends Command
{
    protected static $defaultName = 'postcode:match';
    //protected static $defaultName = 'match';
    
    protected function configure()
    {
        $this
            ->setDescription("Finds postcodes matching a search string, returning them as JSON.")
            
            ->addArgument('query', InputArgument::REQUIRED,
                          "Search string to match against.")
            
            ->addOption('pretty', 'p', InputOption::VALUE_NONE,
                        "Pretty-print JSON.")
            ->addOption('limit', 'l', InputOption::VALUE_REQUIRED,
                        "Max number of results to return.", 20)
            ->addOption('offset', 'o', InputOption::VALUE_REQUIRED,
                        "Value for MySQL's `OFFSET`.", 0)
        ;
    }
    
    protected function interact(InputInterface $input, OutputInterface $output)
    {
        // TODO Maybe prompt user for a search string?
        // TODO Validation? Given the search subject, we could rule out certain queries without even accessing the database...
    }
    
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        
        try {
            // Intentionally not outputting a title so this command's JSON
            //  output can be piped to another command.
            // Writing the title to stderr is an option once I figure out how
            //  to do that - and ideally only if stderr is a tty...
            //$io->title("Postcode Search");
            
            $postcode = new Postcode($input, $output, $io);
            $matches = $postcode->matchPartialString(
                searchQuery: $input->getArgument('query'),
                limit: $input->getOption('limit'),
                offset: $input->getOption('offset'),
            );
            
            $jsonString = Json::encode(
                $matches,
                prettyPrint: $input->getOption('pretty')
            );
            $io->text($jsonString);
            
            return Command::SUCCESS;
        } catch (\Throwable $ex) {
            $io->error($ex->getMessage());
            return Command::FAILURE;
        }
    }
}
