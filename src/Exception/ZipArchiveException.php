<?php

namespace Aziraphale\PassengerTechTask\Exception;

class ZipArchiveException extends \RuntimeException
{
    /**
     * Returns a friendly error string for the passed `ZipArchive::ER_*` const.
     *
     * List of constants and descriptive messages from PhpStorm's Zip "stub":
     *  `/php/lib/php.jar!/stubs/zip/zip.php` inside PhpStorm's plugin dir.
     *
     * @param int    $errorCode
     * @param string $unknownMsg
     * @return string
     */
    public static function errorCodeToMsg(int $errorCode, string $unknownMsg = "Unknown error"): string
    {
        // PHP 7 (unsure if PHP 8 is an option!)
        /*return [
            \ZipArchive::ER_OK          => "No error.",
            \ZipArchive::ER_MULTIDISK   => "Multi-disk zip archives not supported.",
            \ZipArchive::ER_RENAME      => "Renaming temporary file failed.",
            \ZipArchive::ER_CLOSE       => "Closing zip archive failed",
            \ZipArchive::ER_SEEK        => "Seek error",
            \ZipArchive::ER_READ        => "Read error",
            \ZipArchive::ER_WRITE       => "Write error",
            \ZipArchive::ER_CRC         => "CRC error",
            \ZipArchive::ER_ZIPCLOSED   => "Containing zip archive was closed",
            \ZipArchive::ER_NOENT       => "No such file.",
            \ZipArchive::ER_EXISTS      => "File already exists",
            \ZipArchive::ER_OPEN        => "Can't open file",
            \ZipArchive::ER_TMPOPEN     => "Failure to create temporary file.",
            \ZipArchive::ER_ZLIB        => "Zlib error",
            \ZipArchive::ER_MEMORY      => "Memory allocation failure",
            \ZipArchive::ER_CHANGED     => "Entry has been changed",
            \ZipArchive::ER_COMPNOTSUPP => "Compression method not supported.",
            \ZipArchive::ER_EOF         => "Premature EOF",
            \ZipArchive::ER_INVAL       => "Invalid argument",
            \ZipArchive::ER_NOZIP       => "Not a zip archive",
            \ZipArchive::ER_INTERNAL    => "Internal error",
            \ZipArchive::ER_INCONS      => "Zip archive inconsistent",
            \ZipArchive::ER_REMOVE      => "Can't remove file",
            \ZipArchive::ER_DELETED     => "Entry has been deleted",
        ][$errorCode] ?? $unknownMsg;*/
        
        // PHP 8
        return match ($errorCode) {
            \ZipArchive::ER_OK          => "No error.",
            \ZipArchive::ER_MULTIDISK   => "Multi-disk zip archives not supported.",
            \ZipArchive::ER_RENAME      => "Renaming temporary file failed.",
            \ZipArchive::ER_CLOSE       => "Closing zip archive failed",
            \ZipArchive::ER_SEEK        => "Seek error",
            \ZipArchive::ER_READ        => "Read error",
            \ZipArchive::ER_WRITE       => "Write error",
            \ZipArchive::ER_CRC         => "CRC error",
            \ZipArchive::ER_ZIPCLOSED   => "Containing zip archive was closed",
            \ZipArchive::ER_NOENT       => "No such file.",
            \ZipArchive::ER_EXISTS      => "File already exists",
            \ZipArchive::ER_OPEN        => "Can't open file",
            \ZipArchive::ER_TMPOPEN     => "Failure to create temporary file.",
            \ZipArchive::ER_ZLIB        => "Zlib error",
            \ZipArchive::ER_MEMORY      => "Memory allocation failure",
            \ZipArchive::ER_CHANGED     => "Entry has been changed",
            \ZipArchive::ER_COMPNOTSUPP => "Compression method not supported.",
            \ZipArchive::ER_EOF         => "Premature EOF",
            \ZipArchive::ER_INVAL       => "Invalid argument",
            \ZipArchive::ER_NOZIP       => "Not a zip archive",
            \ZipArchive::ER_INTERNAL    => "Internal error",
            \ZipArchive::ER_INCONS      => "Zip archive inconsistent",
            \ZipArchive::ER_REMOVE      => "Can't remove file",
            \ZipArchive::ER_DELETED     => "Entry has been deleted",
            default                     => $unknownMsg,
        };
    }
    
    /**
     * Returns ZipArchiveException with a suitable message for the passed
     * ZipArchive::ER_* const $errorCode
     *
     * @param int $errorCode
     * @return static
     */
    public static function fromZipArchiveErrorCode(int $errorCode): static
    {
        return new static(static::errorCodeToMsg($errorCode), $errorCode);
    }
}
