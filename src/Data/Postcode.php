<?php

namespace Aziraphale\PassengerTechTask\Data;

use Aziraphale\PassengerTechTask\Db;
use Aziraphale\PassengerTechTask\Exception\ZipArchiveException;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class Postcode
{
    /**
     * The column indices (0-indexed) from the "ONS PD" CSV file, so accessing
     *  CSV columns can be a bit clearer.
     * This assumes that the column order in that file never changes. That's
     *  hopefully the case for a dataset like this, where it's expected to be
     *  consumed by automated means, but if it does change frequently, the
     *  header row can be parsed to determine column positions.
     */
    protected const ONSPD_COL = [
        'POSTCODE' => 0,
        'EAST'     => 11,
        'NORTH'    => 12,
        'LAT'      => 42,
        'LON'      => 43,
    ];
    
    /**
     * Download URL for the "Code-Point Open" Zip archive from
     *  https://parlvid.mysociety.org/os/.
     * Temporarily using a local 'file://' URL for testing.
     *
     * @var string
     */
    protected static string $codePointOpenZipDownloadUrl = 'https://parlvid.mysociety.org/os/code-point/codepo_gb-2020-05.zip';
    //protected static string $codePointOpenZipDownloadUrl = 'file:///mnt/c/tmp/passenger/codepo_gb-2020-05.zip';
    
    /**
     * Download URL for the "ONS Postcode Directory (ONSPD)" Zip archive from
     * https://parlvid.mysociety.org/os/.
     * Temporarily using a local 'file://' URL for testing.
     *
     * @var string
     */
    protected static string $onsPostcodeDirectoryZipDownloadUrl = 'https://parlvid.mysociety.org/os/ONSPD/2020-05.zip';
    //protected static string $onsPostcodeDirectoryZipDownloadUrl = 'file:///mnt/c/tmp/passenger/ONSPD_2020-05.zip';
    
    protected array $tempZipFilenames = [];
    
    public function __construct(protected InputInterface $consoleInput,
                                protected OutputInterface $consoleOutput,
                                protected ?SymfonyStyle $consoleIo = null)
    {
        if (!$this->consoleIo) {
            $this->consoleIo = new SymfonyStyle($this->consoleInput, $this->consoleOutput);
        }
    }
    
    public function __destruct()
    {
        // Delete any temporary zip archive download files we created
        foreach ($this->tempZipFilenames as $filename) {
            if (file_exists($filename)) {
                unlink($filename);
            }
        }
    }
    
    /**
     * @todo A progress indicator for this >200 MiB download would be nice...
     * @param string $downloadUrl
     * @return string
     */
    protected function downloadSourceData(string $downloadUrl): string
    {
        // Get a unique temporary filename to write the downloaded data to
        // TODO Make the temp file's parent dir configurable?
        $tempZipFilename = tempnam(sys_get_temp_dir(), 'postcode-data_');
        if ($tempZipFilename === false) {
            throw new \RuntimeException("Unable to obtain temporary file for writing downloaded postcode data archive.");
        }
        
        // Ensure the temporary file is deleted when we've finished
        $this->tempZipFilenames[] = $tempZipFilename;
        
        // Download postcode data (zip archive) to temp file for processing
        if (!copy($downloadUrl, $tempZipFilename)) {
            throw new \RuntimeException("Unable to download postcode data archive from '$downloadUrl' to '$tempZipFilename'.");
        }
        
        return $tempZipFilename;
    }
    
    /**
     * @param string $zipFilename
     * @todo It would probably be faster to parse the source CSV, extract the
     *       data required for the database, write that to a file, and then
     *       use a `LOAD DATA [LOCAL] INFILE '...' INTO TABLE postcode;` query,
     *       possibly running `TRUNCATE postcode;` beforehand.
     *       Whether it's worth the extra complexity depends on the use case,
     *       e.g., how often updates are run and how quickly it needs to finish.
     *       An `awk` script to extract the data and massage it into the format
     *       required by `mysqlimport` took just shy of 4 minutes.
     *       The `mysqlimport` process itself took another 43 seconds.
     *       This PHP-based update method took 9 minutes.
     *       So the awk+mysqlimport option completes in roughly half the time...
     */
    protected function extractAndImportData(string $zipFilename)
    {
        $zip = new \ZipArchive();
        $zipOpenResult = $zip->open($zipFilename);
        if ($zipOpenResult !== true) {
            throw new ZipArchiveException(
                sprintf(
                    "Failed to open downloaded data as a zip archive. ZipArchive error #%d: '%s'.",
                    $zipOpenResult,
                    ZipArchiveException::errorCodeToMsg($zipOpenResult)
                ),
                $zipOpenResult
            );
        }
        
        Db::get()->beginTransaction();
    
        // Prepare SQL statement for importing data.
        // Using "?" style placeholders as numeric arrays seem like they'd be
        //  faster than associative arrays, and we have a massive amount of
        //  data to import here...
        $stmInsert = Db::get()->prepare(/** @lang MariaDB */ <<<EOSQL
            INSERT INTO postcode
                (outcode, incode, eastings, northings, latitude, longitude)
            VALUES
                (?, ?, ?, ?, ?, ?)
            ON DUPLICATE KEY UPDATE
                eastings=VALUES(eastings), northings=VALUES(northings),
                latitude=VALUES(latitude), longitude=VALUES(longitude)
            EOSQL
            );
        
        // Find the appropriate CSV file within the zip archive.
        // The latest "ONSPD" archive uses a file named
        //  "Data/ONSPD_MAY_2020_UK.csv", but that will presumably change in
        //  future updates, so we can't access that filename directly.
        // Is this really how PHP's `Zip` extension expects us to iterate over
        //  archive contents: a `for` loop?
        // RecursiveDirectoryIterator and opendir/readdir in combination with
        //  the `zip://` wrapper just return "not implemented"...
        for ($i=0, $l=$zip->count(); $i < $l; ++$i) {
            // Fetch assoc array of info about this archive entry.
            // Keys: name, index, crc, size, mtime, comp_size, comp_method
            // *NOT* a SplFileInfo instance!
            $fileInfo = $zip->statIndex($i);
            if ($fileInfo) {
                //printf("%d: %s [%d]\n", $i, $fileInfo['name'], $fileInfo['size']);
                
                if (preg_match('#^Data/ONSPD_\w+\.csv$#i', $fileInfo['name'])) {
                    // This is the/a CSV file we want to import.
                    $this->consoleIo->text("Processing file '{$fileInfo['name']}'...");
                    
                    // Obtain a resource we can pass to `fgetcsv()`.
                    $csvStream = $zip->getStream($fileInfo['name']);
    
                    // Use a progress bar with the "max state" at the
                    //  uncompressed size of the CSV file, so we can have a
                    //  decent indication of progress without having to first
                    //  read through the file to count the lines.
                    $this->consoleIo->progressStart($fileInfo['size']);
                    
                    // Skip the CSV's header row
                    fgetcsv($csvStream);
                    $lastProgressPosition = ftell($csvStream);
                    $this->consoleIo->progressAdvance($lastProgressPosition);
                    
                    /** @var array $csvRow */
                    while ($csvRow = fgetcsv($csvStream)) {
                        // The "incode" part of UK postcodes is always 3
                        //  characters matching /^[0-9][A-Z]{2}$/.
                        // The ONSPD dataset has been confirmed to match this.
                        // So we can safely assume the incode to be the last
                        //  3 characters of the postcode column.
                        // (And it should be safe to assume an ASCII-compatible
                        //  character set!)
                        // The "outcode" is right-padded with spaces in the CSV.
                        $postcode = $csvRow[static::ONSPD_COL['POSTCODE']];
                        $outcode = trim(substr($postcode, 0, -3));
                        $incode = substr($postcode, -3);
                        
                        $eastings = $csvRow[static::ONSPD_COL['EAST']];
                        $northings = $csvRow[static::ONSPD_COL['NORTH']];
                        if ($eastings === "") {
                            $eastings = null;
                        }
                        if ($northings === "") {
                            $northings = null;
                        }
                        
                        $lat = $csvRow[static::ONSPD_COL['LAT']];
                        $lon = $csvRow[static::ONSPD_COL['LON']];
    
                        $stmInsert->execute([
                                $outcode,
                                $incode,
                                $eastings,
                                $northings,
                                $lat,
                                $lon,
                            ]);
                        
                        // Advance the progress bar by the number of bytes
                        //  we've read in the CSV file since we last updated
                        //  the progress bar.
                        $currentPosition = ftell($csvStream);
                        $this->consoleIo->progressAdvance($currentPosition - $lastProgressPosition);
                        $lastProgressPosition = $currentPosition;
                    }
                    
                    $this->consoleIo->progressFinish();
                }
            }
        }
        
        $this->consoleIo->text("Committing transaction...");
        Db::get()->commit();
        
        $zip->close();
    }
    
    public function downloadAndImportData()
    {
        //$zipFilename = $this->downloadSourceData(static::$codePointOpenZipDownloadUrl);
        $zipFilename = $this->downloadSourceData(static::$onsPostcodeDirectoryZipDownloadUrl);
        $this->extractAndImportData($zipFilename);
        
        // We've finished with the temporary downloaded-archive file now
        unlink($zipFilename);
    }
    
    /**
     * Cast numeric values to native float/int types to produce more compact
     *  (and sensible) JSON. Also reduces precision of excessive floats.
     *
     * @param array $result
     */
    protected static function castResultsToNativeTypes(array &$result): void
    {
        array_walk($result, function (&$v, $k) {
            $v['latitude'] = (float) $v['latitude'];
            $v['longitude'] = (float) $v['longitude'];
            
            if (isset($v['distance_m'])) {
                $v['distance_m'] = round($v['distance_m'], 2);
            }
        });
    }
    
    /**
     * Returns 2-dimensional array of postcodes that contain $searchQuery (as
     *  a SQL `LIKE %...%` query) along with corresponding latitude+longitude.
     *
     * Inner array has keys: postcode, latitude, longitude
     *
     * @param string $searchQuery
     * @param int    $limit
     * @param int    $offset
     * @return array
     */
    public function matchPartialString(string $searchQuery, int $limit = 20, int $offset = 0): array
    {
        // Matching against the CONCAT() result isn't ideal.
        // May be better to have separate search arguments for outcode and
        //  incode, where one or both can be supplied... (I think that was my
        //  original plan behind separating outcode and incode in the DB)
        $result = Db::get()->fetchAll(
            /** @lang MariaDB */
            <<<EOSQL
                SELECT
                    CONCAT(outcode, incode) AS 'postcode',
                    latitude, longitude
                FROM postcode
                WHERE
                    CONCAT(outcode, incode) LIKE :q
                ORDER BY outcode, incode
                LIMIT :limit OFFSET :offset
            EOSQL,
            ['q' => "%$searchQuery%", 'limit' => $limit, 'offset' => $offset]
        );
        
        static::castResultsToNativeTypes($result);
        return $result;
    }
    
    /**
     * Returns 2-dimensional array of postcodes that are within $distance
     *  metres of the $latitude and $longitude coordinates. The inner returned
     *  array also contains the nominal distance of each postcode to the
     *  specified coordinates, and the postcode's nominal latitude+longitude.
     *
     * Inner array has keys: postcode, latitude, longitude, distance_m
     *
     * @param float $latitude
     * @param float $longitude
     * @param float $distance
     * @param int   $limit
     * @param int   $offset
     * @return array
     * @link https://stackoverflow.com/a/574736
     */
    public function findNearby(float $latitude, float $longitude, float $distance, int $limit = 20, int $offset = 0): array
    {
        // TODO This can (and probably should) be heavily optimised. Running
        //      that calculation on every record before filtering the results
        //      at all is a bit of a nightmare!
        //      Firstly, the sin/cos/acos/radians calculations can be
        //      pre-computed and cached.
        //      https://stackoverflow.com/a/7284463
        //      It may also be possible to narrow the search by using a
        //      faster-but-less-accurate algorithm with a larger search radius,
        //      then running the more-complex Haversine algorithm only on the
        //      points that are known to be somewhat close.
        //      Using MariaDB's built-in geographic/geometric functions may
        //      improve performance, too.
        //      https://mariadb.com/kb/en/geographic-geometric-features/
        // `6371000` is the Earth's radius in metres, giving `distance` results
        //  in metres. Change to `6371` for kilometres, or `3959` for miles.
        $result = Db::get()->fetchAll(
            /** @lang MariaDB */
            <<<EOSQL
                SELECT
                    CONCAT(outcode, incode) AS 'postcode',
                    latitude, longitude,
                    (
                        6371000 *
                        acos(
                            cos(radians(:lat)) *
                            cos(radians(latitude)) *
                            cos(radians(longitude) - radians(:lon)) +
                            sin(radians(:lat)) *
                            sin(radians(latitude))
                        )
                    ) AS distance_m
                FROM postcode
                HAVING distance_m <= :search_radius
                ORDER BY distance_m
                LIMIT :limit OFFSET :offset
            EOSQL,
            [
                'lat'           => $latitude,
                'lon'           => $longitude,
                'search_radius' => $distance,
                'limit'         => $limit,
                'offset'        => $offset,
            ]
        );
        
        static::castResultsToNativeTypes($result);
        return $result;
    }
}
