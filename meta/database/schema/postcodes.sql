-- postcodes table:
--  *outcode [char(4) ascii]
--  *incode [char(3)? ascii]
--   eastings [int(6) (unsigned) zerofill]
--   northings [int(7) (unsigned) zerofill]
--   latitude [float(8,6)]
--   longitude [float(8,6)]
--
-- outcode+incode could probably be PK

CREATE TABLE postcode (
    outcode     CHAR(4) CHARACTER SET ascii NOT NULL,
    incode      CHAR(3) CHARACTER SET ascii NOT NULL,
    eastings    MEDIUMINT(6) ZEROFILL           NULL,
    northings   MEDIUMINT(7) ZEROFILL           NULL,
    latitude    FLOAT(8, 6)                 NOT NULL,
    longitude   FLOAT(8, 6)                 NOT NULL,
    PRIMARY KEY (outcode, incode),
    INDEX (latitude),
    INDEX (longitude)
) ENGINE=InnoDB;
