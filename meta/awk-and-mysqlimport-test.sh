#!/usr/bin/env sh

# Extract the required CSV columns, splitting postcode into outcode+incode, and replacing empty strings for "eastings" and "northings" with "\N" (NULL).
# (The `sub(/^"/, "", $1);` line removes the leading `"` of the first field. Using a field separator with optional `"` characters doesn't apply to the first or last fields.)
awk -v FS='"?,"?' 'FNR>=2 {OFS=","; sub(/^"/, "", $1); outcode=substr($1, 1, length($1)-3); outcode=gensub(/ /, "", "g", outcode); incode=substr($1, length($1)-2); east=($12=="" ? "\\\\N" : $12); north=($13=="" ? "\\\\N" : $13); print outcode, incode, east, north, $43, $44}' ONSPD_MAY_2020_UK.csv > ONSPD_MAY_2020_UK__massaged-via-awk.csv

mysqlimport -r -c "outcode,incode,eastings,northings,latitude,longitude" --local --fields-terminated-by="," -u [USERNAME] -p [DB-NAME] postcode.txt


# Timings:
# PHP, bound parameters, transaction = 8:56 mins
# PHP, bound parameters, NO transaction = 9:22 mins
# AWK command: 3:50 mins
# mysqlimport command: 0:43 mins

# PHP import ran at ~10MB/s WITHOUT DB queries; ~3MB/s WITH DB queries.
# Different SQL INSERT-related statements made no significant difference, nor did using `bindParam()` instead of passing values to PDO's `execute()`.
