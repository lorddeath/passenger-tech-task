#!/usr/bin/php
<?php

use Aziraphale\PassengerTechTask\Command\MatchCommand;
use Aziraphale\PassengerTechTask\Command\NearbyPostcodesCommand;
use Aziraphale\PassengerTechTask\Command\UpdateCommand;
use Aziraphale\PassengerTechTask\Db;
use Aziraphale\PassengerTechTask\Helper\ConsoleHelper;
use Symfony\Component\Console\Application;

require __DIR__ . '/vendor/autoload.php';

// Ensure that our shutdown functions and class destructors are called if we
//  receive a SIGINT/SIGTERM signal (*nix) or a CTRL+C event (Windows).
// Symfony's Console Component has similar functionality, but it seems to be
//  *nix-only and using their implementation would entirely prevent running
//  this script under Windows (not simply allowing it to be run without the
//  CTRL+C handler). Eliminating Windows support just for this isn't ideal.
ConsoleHelper::setupCleanExitOnSigintAndSigterm();

// PDO supports `user`+`password` in MySQL's DSN as of PHP 7.4, despite
//  this not being mentioned in the docs...
// TODO Move database connection details to a config file or something
Db::init(
    'mysql:host=[HOST];user=[USERNAME];password=[PASSWORD];dbname=[DB-NAME];charset=utf8mb4',
);

$postcodeApp = new Application("Postcode Command App");

$postcodeApp->add(new UpdateCommand());
$postcodeApp->add(new MatchCommand());
$postcodeApp->add(new NearbyPostcodesCommand());

$postcodeApp->run();
